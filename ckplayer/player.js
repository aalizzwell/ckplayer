﻿var Player = {
    container: null,
    id: null,
    width: 700,
    height: 572,
    counter: 0,
    currentPrevLogo: null,
    loadCompleteCallBack: new Array(),
    loadComplete: function () {
        for (var i in Player.loadCompleteCallBack) {
            Player.loadCompleteCallBack[i]();
        }
    },
    init: function (sContainer, sId) {
        Player.container = sContainer, Player.id = sId;
    },
    loadEngine: function (callback) {
        Player.loadCompleteCallBack.push(callback);
        var flashvars = { f: '', c: 0, b: 0, p: 0, m: 1, wh: '11:9' };
        if (document.getElementById(Player.id) == undefined)
            CKobject.embedSWF('ckplayer/ckplayer.swf', Player.container, Player.id, Player.width, Player.height, flashvars);
        else
            Player.loadComplete();
    },
    beginPlay: function (sStreamUrl) {
        Player.loadEngine(function () {
            var thePlayer = CKobject.getObjectById(Player.id);
            thePlayer.ckplayer_newaddress("{f->" + sStreamUrl + "}");
            thePlayer.ckplayer_playorpause();
        });
    },
    stopPlay: function () {
        var theContainer = document.getElementById(Player.container);
        theContainer.removeChild(document.getElementById(Player.id));
    }
};
function ckplayer_status(status) {
    if (status.toLowerCase() == "plug:load.ok") {
        Player.counter++;
        if (Player.counter >= 5) {
            Player.loadComplete();
            Player.counter = 0;
        }
    }
}
document.oncontextmenu = function () { return false; }

